---
title: "Nevýchova: úvodní seriál"
date: 2018-07-08T12:22:34+06:00
author: "Katka Králová"
link: "https://www.nevychova.cz/"
cta: "Shlédnout"
linkType: "Video"
linkTypeName: "Video"
---

Nevýchova je přístup k rodičovství, který se v mnoha aspektech shoduje s Nenásilnou komunikací. V sérii videí se jasně představí stěžejní principy, na kterých je přístup postavený. Pro hlubší pochopení metody se nabízí podrobnější placený kurz.