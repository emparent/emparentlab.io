---
title: "Nonviolent Communication Workshop"
date: 2018-07-08T12:22:40+06:00
author: Marshall Rosenberg
link: "https://youtu.be/l7TONauJGfc"
cta: "Watch now"
linkType: "Video"
linkTypeName: "Video"
---

Marshall Rosenberg, the inventor of Nonviolent Communication (NVC), presents a general, yet thorough introduction of his method. The workshop is based around NVC in all types of communication, not just parent-child communication.