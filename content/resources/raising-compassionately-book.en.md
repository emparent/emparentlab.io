---
title: "Raising Children Compassionately"
date: 2018-07-08T12:22:40+06:00
author: Marshall Rosenberg
link: "https://cnvc-bookstore.myshopify.com/collections/parenting/products/respectful-parents-respectful-kids"
cta: "Buy"
linkType: "Book"
linkTypeName: "Booklet"
---

In just 48 pages, Marshall makes a case for a parenting approach based on empathy and presents multiple stories of how it works in practices.
