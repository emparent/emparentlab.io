---
title: "Respektovat a být respektován"
date: 2018-04-08T12:22:40+06:00
author: "Pavel Kopřiva, Jana Nováčková, Dobromila Nevolová, Tatjana Kopřivová"
link: "https://www.zbozi.cz/vyrobek/respektovat-a-byt-respektovan/"
cta: "Koupit"
linkType: "Book"
linkTypeName: "Knížka"
---

Knížka českých autorů o tom, jak nejlépe komunikovat s dětmi, abyste dosáhli vzájemného porozumění