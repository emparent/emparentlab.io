---
title: "Nenásilná komunikace"
date: 2018-04-08T12:22:40+06:00
author: Marshall Rosenberg
link: "https://www.zbozi.cz/vyrobek/nenasilna-komunikace-marshall-b-rosenberg/"
cta: "Koupit"
linkType: "Book"
linkTypeName: "Knížka"
---

Základní kniha metody Nenásilné komunikace, kde její autor popisuje a ukazuje na příkladech, co Nenásilná komunikace je a jak lze aplikovat v různých situacích (nejen mezi rodiči a dětmi).