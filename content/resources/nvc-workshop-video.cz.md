---
title: "Workshop nenásilné komunikace (v AJ se slovenskými titulky)"
date: 2018-07-08T12:22:40+06:00
author: Marshall Rosenberg
link: "https://youtu.be/l7TONauJGfc"
cta: "Shlédnout"
linkType: "Video"
linkTypeName: "Video"
---

Jde o obecné představení metodiky Nenásilné komunikace jejím zakladatelem Marshallem Rosenbergem. Workshop jasně předkládá stěžejní koncepty procesu Nenásilné komunikace. Je zaměřen na konflikty všech typů, nejen na rodičovské problémy.